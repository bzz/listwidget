package guta.listwidget;

import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;

import guta.listwidget.manager.Postman;
import guta.listwidget.manager.FileManager;


public class RemoteFetchService extends Service {
    public final static String TAG = "RemoteFetchService";
    public static ArrayList<ListItem> listItemList;
    private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.hasExtra(AppWidgetManager.EXTRA_APPWIDGET_ID))
            appWidgetId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);

        fetchDataFromWeb();

        return super.onStartCommand(intent, flags, startId);
    }


    public void fetchDataFromWeb() {
        String result = "";

/* TODO: Remove the hack making possible internet connections from the main thread */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        if ( !(Postman.canConnectContext(getApplicationContext()))) return;


        result = Postman.readFromURL(Postman.SERVER_URL).trim();
        //result = Postman.post();


///get json from file
//        result = FileManager.readStream(getBaseContext().getResources().openRawResource(R.raw.json_example));

        Log.i(TAG, "RESULT.trim():" + result);
        if (result != null) {
            FileManager.writeToFile(getApplicationContext(), result, "text.json");
        } else {
            result = FileManager.readFromFile(getApplicationContext(), "text.json");
        }
        processResult(result);
    }


    private void processResult(String result) {
        listItemList = new ArrayList<ListItem>();

        try {
            JSONObject obj = new JSONObject(result);
            for(Iterator<String> keys=obj.keys();keys.hasNext();) {
                String title = keys.next().toString();
                JSONArray jsonArray =  obj.getJSONArray(title);
                ListItem item = new ListItem(title);
                for(int n = 0; n < jsonArray.length(); n++) {
                    JSONObject object = jsonArray.getJSONObject(n);
                    Log.i(TAG, object.toString());

                    if (object.has("title") && object.has("string")) {
                        item.addSubitem(object.get("title").toString(),
                                object.getString("string".toString()));
                    }
                }
                listItemList.add(item);
            }
        } catch (JSONException e) {e.printStackTrace();}

        populateWidget();
    }


    private void populateWidget() {
        Intent updateIntent = new Intent();
        updateIntent.setAction(WidgetProvider.DATA_FETCHED);
        updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        sendBroadcast(updateIntent);
        this.stopSelf();
    }





}
