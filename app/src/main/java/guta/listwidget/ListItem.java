package guta.listwidget;

import java.util.ArrayList;


public class ListItem {
    private ArrayList<Listsubitem> subitems = new ArrayList<>();
    public String title = "";

    ListItem(String title) {
        this.title = title;
    }

    ListItem() {
    }

    public void addSubitem(String key, String value) {
        Listsubitem subitem = new Listsubitem(key, value);
        this.subitems.add(subitem);
    }

    public ArrayList<Listsubitem> getSubitems() {
        return subitems;
    }

}


