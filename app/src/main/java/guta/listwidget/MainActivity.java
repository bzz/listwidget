package guta.listwidget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private Button btn1;
    private Button btn2;
    private Button btn3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        layoutButtons();
        this.btn2.setOnClickListener(
                new Button.OnClickListener() {
                    public void onClick(View v) {
//                        Intent i = new Intent(MainActivity.this, SecondActivity.class);
//                        startActivity(i);
                    }
                }
        );
    }


    private void layoutButtons() {
//layout
        RelativeLayout layout = new RelativeLayout(this);
        layout.setBackgroundColor(Color.WHITE);

//btn1
        this.btn1 = new Button(this);
        this.btn1.setText("Обсуждения");
        this.btn1.setId(View.generateViewId());
        RelativeLayout.LayoutParams btn1param = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        btn1param.addRule(RelativeLayout.CENTER_VERTICAL);
        btn1param.addRule(RelativeLayout.CENTER_HORIZONTAL);

//btn2
        this.btn2 = new Button(this);
        this.btn2.setText("Уведомления");
        this.btn2.setId(View.generateViewId());
        RelativeLayout.LayoutParams btn2param = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        btn2param.addRule(RelativeLayout.CENTER_HORIZONTAL);
        btn2param.addRule(RelativeLayout.ABOVE, this.btn1.getId());

//btn3
        this.btn3 = new Button(this);
        this.btn3.setText("Карта");
        this.btn3.setId(View.generateViewId());
        RelativeLayout.LayoutParams btn3param = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        btn3param.addRule(RelativeLayout.CENTER_HORIZONTAL);
        btn3param.addRule(RelativeLayout.ABOVE, this.btn2.getId());


        layout.addView(this.btn1, btn1param);
        layout.addView(this.btn2, btn2param);
        layout.addView(this.btn3, btn3param);

        setContentView(layout);
    }
}
