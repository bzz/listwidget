package guta.listwidget;

import java.util.ArrayList;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;



public class ListProvider implements RemoteViewsFactory {
    private ArrayList<ListItem> items = new ArrayList<ListItem>();



    private Context context = null;
    private int appWidgetId;

    public ListProvider(Context context, Intent intent) {
        this.context = context;
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        items = (ArrayList<ListItem>) RemoteFetchService.listItemList.clone();
    }



    @Override
    public RemoteViews getViewAt(int position) {
        ListItem item = items.get(position);
        RemoteViews itemView = new RemoteViews(context.getPackageName(), R.layout.widget_listview_item);
        itemView.removeAllViews(R.id.linearLayout_item_body);
        itemView.setTextViewText(R.id.textView_title, item.title);

        for (Listsubitem s : item.getSubitems()) {
            RemoteViews subitem = new RemoteViews(context.getPackageName(), R.layout.widget_listview_subitem);
            subitem.setTextViewText(R.id.textView_1, s.key);
            subitem.setTextViewText(R.id.textView_2, s.value);
            itemView.addView(R.id.linearLayout_item_body, subitem);

            //TODO: Clicking list item
            Intent fillInIntent = new Intent();
            fillInIntent.putExtra(WidgetProvider.EXTRA_ITEM, position);
            itemView.setOnClickFillInIntent(R.id.widget_root, fillInIntent);

        }
        return itemView;
    }



    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDataSetChanged() {
    }

    @Override
    public void onDestroy() {
    }

}