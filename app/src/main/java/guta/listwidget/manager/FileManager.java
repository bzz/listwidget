package guta.listwidget.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import android.content.Context;
import android.util.Log;


public enum FileManager {
    INSTANCE;

    public static void writeToFile(Context context, String text, String fileName) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
            outputStreamWriter.write(text);
            outputStreamWriter.close();
        } catch (IOException e) {Log.e("Exception", "writeToFile " + e.toString());}
    }

    public static String readFromFile(Context context, String fileName) {
        File file = new File(context.getFilesDir(), fileName);
        int length = (int) file.length();
        byte[] bytes = new byte[length];
        try {
            FileInputStream in = new FileInputStream(file);
            try {
                in.read(bytes);
            } finally {
                in.close();
            }
        } catch (IOException e) {Log.e("Exception", "readFromFile " + e.toString());}
        return new String(bytes);
    }

    public static String readStream(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {Log.e("Exception", "readStream " + e.toString());}
        finally {
            try {
                is.close();
            } catch (IOException e) {Log.e("Exception", "readStream " + e.toString());}
        }
        return sb.toString();
    }

}

