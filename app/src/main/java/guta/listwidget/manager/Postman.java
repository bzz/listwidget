package guta.listwidget.manager;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;


public enum Postman {
    INSTANCE;

//    public static final String SERVER_HOST =  "dashboard.gutagroup.ru/api/account/login";

    public static final String SERVER_HOST =  "rpix.herokuapp.com";//"btc-e.com/api/3/ticker/btc_rur"; //"rpix.herokuapp.com"; //"dashboard.gutagroup.ru"
    public static final String SERVER_URL = "https://" + SERVER_HOST;

    public static final int SERVER_PORT = 80;


    public static boolean canConnectContext(Context context) {
        try {
            if ( !(Postman.isConnected(context)) ) {
                Toast.makeText(context, "CONNECTION ERROR", Toast.LENGTH_SHORT).show();
                return false;
            }
            /*
            else if ( !(Postman.isHostReachable(Postman.SERVER_HOST, Postman.SERVER_PORT, 20000)) ) {
                Toast.makeText(context, "SERVER NOT REACHABLE", Toast.LENGTH_SHORT).show();
                return false;
            }
            */
            return true;
        } catch (Exception e) {Log.e("Exception", "canConnectContext " + e.toString());
            return false;
        }
    }

    public static String readFromURL(String urlStr) {
        String out = new String();
        try {
            URL url = new URL(urlStr);
            URLConnection urlConnection = url.openConnection();
            InputStream is = new BufferedInputStream(urlConnection.getInputStream());
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader r = new BufferedReader(new InputStreamReader(is),1000);
                for (String line = r.readLine(); line != null; line =r.readLine()){
                    sb.append(line);
                }
            } finally {
                is.close();
                out = sb.toString();
            }
        } catch (IOException e) {Log.e("Exception", "readFromURL " + e.toString());}
        return out;
    }


    public static String post() {
        String out = new String();

        HashMap map = new HashMap<String, String>();
        map.put("login", "local");
        map.put("password", "qwerty");
        JSONObject auth=new JSONObject(map);
        return performPost(SERVER_URL, auth);
    }

    public static String performPost(String requestURL, JSONObject json) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setReadTimeout(15000);
            con.setConnectTimeout(15000);

            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");

            con.setDoInput(true);
            con.setDoOutput(true);


            OutputStream wr = con.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(wr, "UTF-8"));
            writer.write(json.toString());

            writer.flush();
            writer.close();
            wr.close();

            int responseCode=con.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            } else {
                response="";
            }

//Get cookies
            Map<String, List<String>> headerFields = con.getHeaderFields();
            List<String> cookiesHeader = headerFields.get("Set-Cookie");
            if(cookiesHeader != null)
            {
                String name = "";
                String value = "";
                for (String cookie : cookiesHeader)
                {
                    HttpCookie c = HttpCookie.parse(cookie).get(0);
                    name = c.getName();
                    value = c.getValue();
                }
                Log.i("COOKIE","NAME: " + name + "\n  VALUE: " + value);
            }
        } catch (IOException e) {Log.e("Exception", "performPost " + e.toString());}
        return response;
    }

    
    private static boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

/*
    private static boolean isHostReachable(String serverAddress, int serverTCPport, int timeoutMS) {
        boolean connected = false;
        Socket socket;
        try {
            socket = new Socket();
            SocketAddress socketAddress = new InetSocketAddress(serverAddress, serverTCPport);
            socket.connect(socketAddress, timeoutMS);
            if (socket.isConnected()) {
                connected = true;
                socket.close();
            }
        } catch (IOException e) {Log.e("Exception", "isHostReachable " + e.toString());}
        return connected;
    }
*/
}
